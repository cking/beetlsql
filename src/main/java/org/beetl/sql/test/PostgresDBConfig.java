package org.beetl.sql.test;

public class PostgresDBConfig {
	public static String driver = "org.postgresql.Driver";
    public static String dbName = "postgres";
    public static String password = "postgres";
    public static String userName = "postgres";
    public static String url = "jdbc:postgresql://127.0.0.1:5432/postgres";

}
