package org.beetl.sql;

public class OracleDBConfig {
	public static String driver = "oracle.jdbc.driver.OracleDriver";
    public static String dbName = "test";
    public static String password = "test";
    public static String userName = "test";
    public static String url = "jdbc:oracle:thin:@localhost:1521:orcl";

}
